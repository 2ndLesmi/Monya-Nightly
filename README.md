# Monya-Nightly
The unofficial nightly build of [Monya](https://github.com/monya-wallet/monya).     
Don't trust this repo unless you have a BIG brave.    
[Check it out here](https://gitlab.com/2ndLesmi/Monya-Nightly/pipelines) for builds.     
    
[もにゃ](https://github.com/monya-wallet/monya)の非公式Nightly buildです。    
変な勇気がある方以外はここを信用しないでください。    
[ビルドの一覧はここ](https://gitlab.com/2ndLesmi/Monya-Nightly/pipelines)にあります。     
